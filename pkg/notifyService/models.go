package notifyService

type ConnData struct {
	ApiPublic string
	ApiSecret string
}

type NotifyParams struct {
	URL        string `json:"url"`
	SiteStatus string `json:"site_status"`
}
