package notifyService

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"health_checker/config"
)

type client struct {
	httpClient *resty.Client
	cfg        *config.Config
}

func NewNotifyServiceClient(cfg *config.Config) Client {
	return &client{
		httpClient: resty.New().EnableTrace().SetDebug(false),
		cfg:        cfg,
	}
}

func (c client) SendNotify(params *NotifyParams) error {

	bodyRaw, err := json.Marshal(params)
	if err != nil {
		return fmt.Errorf(" Marshal -> '%s' -> params: [%v]", err.Error(), params)
	}

	response, err := c.httpClient.R().
		SetHeader("Content-Type", "application/json").
		SetBody(string(bodyRaw)).
		Post(c.cfg.NotifyService.Url + "/post")

	if err != nil {
		return fmt.Errorf("NotifyService -> %s", err.Error())
	}
	if response.StatusCode() != 200 {
		errorMessage := fmt.Sprintf("NotifyService (%d)\nUrl:%s", response.StatusCode(), c.cfg.NotifyService.Url+"/post")
		return errors.New(errorMessage)
	}
	return nil
}
