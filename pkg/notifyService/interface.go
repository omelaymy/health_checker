package notifyService

type Client interface {
	SendNotify(params *NotifyParams) error
}
