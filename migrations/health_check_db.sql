create table currencies
(
    id serial not null constraint currencies_pkey primary key,
    url text not null,
    create_time timestamp not null,
    text text not null,
    status_code text not null,
    site_status text not null
);

alter table currencies owner to postgres;

