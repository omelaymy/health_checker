package usecase

import (
	"fmt"
	"health_checker/config"
	"health_checker/internal/checker"
	"health_checker/pkg/notifyService"
	"health_checker/pkg/utils"
	"log"
	"net/http"
	"strings"
)

type CheckerUseCase struct {
	cfg             *config.Config
	CashCheckStatus map[string]map[string]string
	CashSiteStatus  map[string]string
	notifyClient    notifyService.Client
	checkerRepo     checker.CheckerRepository
}

func NewCheckerUC(cfg *config.Config, notifyClient notifyService.Client, checkerRepo checker.CheckerRepository) checker.UseCase {
	return &CheckerUseCase{
		cfg:             cfg,
		CashCheckStatus: make(map[string]map[string]string),
		CashSiteStatus:  make(map[string]string),
		notifyClient:    notifyClient,
		checkerRepo:     checkerRepo,
	}
}

func (c *CheckerUseCase) HealthCheck() {
	cfgParams := c.cfg.SitesToCheck
	sitesToCheck := &checker.SitesToCheck{
		CheckParams: cfgParams.Urls,
	}

	for _, params := range sitesToCheck.CheckParams {
		c.checkSite(params.URL, params.Checks, params.MinChecksCnt)
	}
}

func (c *CheckerUseCase) checkSite(url string, checks []string, minCheckCnt int64) {
	var client = &http.Client{}
	var siteStatus string

	timeCheck := utils.GetEuropeTime()

	err := c.checkerRepo.InsertCheckSite(url, timeCheck)
	if err != nil {
		log.Printf("InsertCheckSite error -> %v", err)
	}

	_, ok := c.CashSiteStatus[url]
	if !ok {
		c.CashSiteStatus[url] = "ok"
	}

	response, err := client.Get(url)
	if err != nil {
		log.Printf("request error: %v", err)
	}
	for _, checkType := range checks {

		_, ok = c.CashCheckStatus[url]
		if !ok {
			c.CashCheckStatus[url] = make(map[string]string, 0)
		}
		_, ok = c.CashCheckStatus[url][checkType]
		if !ok {
			c.CashCheckStatus[url][checkType] = "ok"
		}

		switch checkType {
		case "status_code":
			if response.StatusCode != 200 {
				c.CashCheckStatus[url][checkType] = "fail"
				fmt.Printf("Site: %s StatusCode: %d\n", url, response.StatusCode)
			} else {
				c.CashCheckStatus[url][checkType] = "ok"
				minCheckCnt -= 1
				fmt.Printf("Site: %s StatusCode: %d\n", url, response.StatusCode)
			}
			err = c.checkerRepo.SetCheckResult(&checker.SetCheckResult{
				URL:       url,
				CheckType: checkType,
				TimeCheck: timeCheck,
				Result:    c.CashCheckStatus[url][checkType],
			})
			if err != nil {
				log.Printf("SetCheckResult error: -> %v", err)
			}
		case "text":
			if !strings.Contains(response.Status, "OK") {
				c.CashCheckStatus[url][checkType] = "fail"
				fmt.Printf("Site: %s Text: %s\n", url, response.Status)
			} else {
				c.CashCheckStatus[url][checkType] = "ok"
				minCheckCnt -= 1
				fmt.Printf("Site: %s Text: OK\n", url)
			}
			if err != nil {
				fmt.Printf("SetSiteCheckResult error: -> %v", err)
			}
			err = c.checkerRepo.SetCheckResult(&checker.SetCheckResult{
				URL:       url,
				CheckType: checkType,
				TimeCheck: timeCheck,
				Result:    c.CashCheckStatus[url][checkType],
			})
			if err != nil {
				log.Printf("SetSiteCheckResult error: -> %v", err)
			}

		default:
			continue
		}
	}
	if minCheckCnt > 0 {
		siteStatus = "fail"
	} else {
		siteStatus = "ok"
	}
	err = c.checkerRepo.SetSiteStatus(url, siteStatus, timeCheck)
	if err != nil {
		log.Printf("SetSiteStatus error: -> %v", err)
	}
	if c.CashSiteStatus[url] != siteStatus {
		c.CashSiteStatus[url] = siteStatus
		err = c.notifyClient.SendNotify(&notifyService.NotifyParams{
			URL:        url,
			SiteStatus: siteStatus,
		})
		if err != nil {
			fmt.Printf("SendNotify error: -> %v", err)
		}
	}
}

func (c *CheckerUseCase) GetCheckResult(siteUrl string) (*[]checker.GetCheckResultResponse, error) {
	return c.checkerRepo.GetCheckResult(siteUrl)
}
