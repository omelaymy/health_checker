package checker

type UseCase interface {
	HealthCheck()
	GetCheckResult(siteUrl string) (*[]GetCheckResultResponse, error)
}
