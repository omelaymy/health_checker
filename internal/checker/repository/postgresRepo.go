package repository

import (
	"errors"
	"github.com/jmoiron/sqlx"
	"health_checker/internal/checker"
	"time"
)

type postgresRepository struct {
	db *sqlx.DB
}

func NewPostgresRepository(db *sqlx.DB) checker.CheckerRepository {
	return &postgresRepository{
		db: db,
	}
}

func (p postgresRepository) InsertCheckSite(url string, timeCheck time.Time) error {
	conn, err := p.db.Query(`INSERT INTO check_result (url, time_check) VALUES($1, $2)`, url, timeCheck)
	if err != nil {
		return err
	}
	_ = conn.Close()
	return nil
}

func (p postgresRepository) SetCheckResult(params *checker.SetCheckResult) error {
	var query string
	if params.CheckType == "text" {
		query = `UPDATE check_result text=$1 WHERE url=$2 AND time_check=$3`
	} else if params.CheckType == "status_code" {
		query = `UPDATE check_result status_code=$1 WHERE url=$2 AND time_check=$3`
	} else {
		return errors.New("invalid check type")
	}
	conn, err := p.db.Query(query, params.Result, params.URL, params.TimeCheck)
	if err != nil {
		return err
	}
	_ = conn.Close()
	return nil
}

func (p postgresRepository) SetSiteStatus(url, siteStatus string, timeCheck time.Time) error {
	conn, err := p.db.Query(`UPDATE check_result SET site_status=$1 WHERE url=$2 AND time_check=$3`,
		siteStatus, url, timeCheck)
	if err != nil {
		return err
	}
	_ = conn.Close()
	return nil
}

func (p postgresRepository) GetCheckResult(siteUrl string) (*[]checker.GetCheckResultResponse, error) {
	var data []checker.GetCheckResultResponse
	err := p.db.Select(&data, "SELECT * FROM services WHERE url=$1", siteUrl)
	if err != nil {
		return nil, err
	}
	return &data, nil
}
