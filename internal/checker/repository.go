package checker

import "time"

type CheckerRepository interface {
	InsertCheckSite(url string, timeCheck time.Time) error
	SetCheckResult(params *SetCheckResult) error
	SetSiteStatus(url, siteStatus string, timeCheck time.Time) error
	GetCheckResult(siteUrl string) (*[]GetCheckResultResponse, error)
}
