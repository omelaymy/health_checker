package http

import (
	"github.com/gofiber/fiber/v2"
	"health_checker/config"
	"health_checker/internal/checker"
)

type CheckerHandlers struct {
	cfg       *config.Config
	checkerUC checker.UseCase
}

func NewCheckerHandlers(cfg *config.Config, checkerUC checker.UseCase) *CheckerHandlers {
	return &CheckerHandlers{
		cfg:       cfg,
		checkerUC: checkerUC,
	}
}

func (h *CheckerHandlers) CheckResult() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		siteUrl := ctx.Params("url")
		data, err := h.checkerUC.GetCheckResult(siteUrl)
		if err != nil {
			return err
		}
		return ctx.JSON(data)
	}
}
