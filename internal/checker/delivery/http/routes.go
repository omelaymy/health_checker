package http

import (
	"github.com/gofiber/fiber/v2"
)

func MapCheckerRoutes(router fiber.Router, h *CheckerHandlers) {
	router.Get("/check_result", h.CheckResult())
}
