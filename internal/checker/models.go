package checker

import "time"

type SitesToCheck struct {
	CheckParams []struct {
		URL          string   `json:"url"`
		Checks       []string `json:"checks"`
		MinChecksCnt int64    `json:"min_checks_cnt"`
	} `json:"check_params"`
}

type SetCheckResult struct {
	URL       string    `db:"url"`
	CheckType string    `db:"check_type"`
	Result    string    `db:"result"`
	TimeCheck time.Time `db:"time_check"`
}

type GetCheckResultResponse struct {
	URL        string    `json:"url" db:"url"`
	StatusCode string    `json:"status_code" db:"status_code"`
	Text       string    `json:"text" db:"text"`
	TimeCheck  time.Time `json:"time_check" db:"time_check"`
	SiteStatus string    `json:"site_status" db:"site_status"`
}
