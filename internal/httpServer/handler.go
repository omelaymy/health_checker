package httpServer

import (
	"github.com/gofiber/fiber/v2"
	"time"

	checkerHttp "health_checker/internal/checker/delivery/http"
	checkerRepository "health_checker/internal/checker/repository"
	checkerUseCase "health_checker/internal/checker/usecase"
	"health_checker/pkg/notifyService"
	"health_checker/pkg/storage"
)

func (s *Server) MapHandlers(app *fiber.App) error {

	postgreConnection, err := storage.InitPsqlDB(s.cfg)
	if err != nil {
		return err
	}

	notifyClient := notifyService.NewNotifyServiceClient(s.cfg)

	checkerRepo := checkerRepository.NewPostgresRepository(postgreConnection)
	checkerUC := checkerUseCase.NewCheckerUC(s.cfg, notifyClient, checkerRepo)
	checkerHandlers := checkerHttp.NewCheckerHandlers(s.cfg, checkerUC)

	checkerHttp.MapCheckerRoutes(app, checkerHandlers)

	for {
		checkerUC.HealthCheck()
		time.Sleep(time.Second * 15)
	}

	return nil
}
