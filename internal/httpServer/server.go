package httpServer

import (
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"health_checker/config"
)

// Server struct
type Server struct {
	fiber *fiber.App
	cfg   *config.Config
}

func NewServer(cfg *config.Config) *Server {
	return &Server{
		fiber: fiber.New(fiber.Config{DisableStartupMessage: true}),
		cfg:   cfg,
	}
}

func (s *Server) Run() error {
	if err := s.MapHandlers(s.fiber); err != nil {
		return errors.New("Cannot map handlers: ")
	}
	if err := s.fiber.Listen(fmt.Sprintf("%s:%s", s.cfg.Server.Host, s.cfg.Server.Port)); err != nil {
		return errors.New("Error starting Server: ")
	}

	return nil
}
