package config

import (
	"github.com/spf13/viper"
	"log"
)

type Config struct {
	SitesToCheck  SitesToCheckConfig
	System        SystemConfig
	Server        ServerConfig
	Postgres      PostgresConfig
	NotifyService NotifyServiceConfig
}

type PostgresConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"-"`
	DBName   string `json:"DBName"`
	SSLMode  string `json:"sslMode"`
	PgDriver string `json:"pgDriver"`
}

type SystemConfig struct {
	MaxGoRoutines int64
}

type ServerConfig struct {
	AppVersion string `json:"appVersion"`
	Host       string `json:"host"`
	Port       string `json:"port"`
}

type SitesToCheckConfig struct {
	Urls []struct {
		URL          string   `json:"url"`
		Checks       []string `json:"checks"`
		MinChecksCnt int64    `json:"min_checks_cnt"`
	} `json:"urls"`
}

type NotifyServiceConfig struct {
	Url string `json:"url"`
}

func LoadConfig() (*viper.Viper, error) {

	v := viper.New()
	v.AddConfigPath("config") // path for debug
	v.SetConfigName("config")
	v.SetConfigType("yml")
	err := v.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return v, nil
}

func ParseConfig(v *viper.Viper) (*Config, error) {
	var config Config

	if err := v.Unmarshal(&config); err != nil {
		log.Fatalf("unable to decode config into struct, %v", err)
		return nil, err
	}
	return &config, nil
}
