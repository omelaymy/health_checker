package main

import (
	"health_checker/config"
	"health_checker/internal/httpServer"
	"log"
)

func main() {

	log.Println("Starting server")

	v, err := config.LoadConfig()
	if err != nil {
		log.Fatalf("Cannot load config: %s", err.Error())
	}
	log.Println("Config loaded")
	cfg, err := config.ParseConfig(v)
	if err != nil {
		log.Fatalf("Config parse error: %v", err.Error())
	}
	log.Println("Config success")

	s := httpServer.NewServer(cfg)
	err = s.Run()
	if err != nil {
		log.Fatalf("Server shutdown with error: %v", err)
	}
}
